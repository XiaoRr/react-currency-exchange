import React, {Component} from 'react';
import './App.less';
import InputBox from "./components/InputBox";
import GuessList from "./components/GuessList";
import Axios from "axios";
class App extends Component{

  constructor(props) {
    super(props);
    this.state = {
      gameId : -1
    }
  }

  startGameHandler = () => {
    Axios.post("http://localhost:8080/api/games")
      .then(data => {
        this.state.gameId = data.headers.location;
        console.log(this.state.gameId)
      })
      .catch(err => console.log(err));
  };


  render() {
    return (
      <div className = 'App' >
        <button onClick={this.startGameHandler}>Start new game</button>
        <InputBox />
        <GuessList />
      </div>
    );
  }x
}

// <image className="win" src="" />
export default App;